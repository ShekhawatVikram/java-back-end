package com.app.util;

import com.app.models.ClinicalData;

import java.util.List;

public class BMIcalculator {
    public static  void bmiCalculator(List<ClinicalData> clinicalData, ClinicalData data) {
        if(data.getComponentName().equals("hw")){
            String[] hw=data.getComponentValue().split("/");
            if(hw!=null && hw.length>1) {
                float heightInMetre = Float.parseFloat(hw[0]) * 0.4536F;
                float bmi = Float.parseFloat(hw[1]) / (heightInMetre * heightInMetre);
                ClinicalData bmiData = new ClinicalData();
                bmiData.setComponentName("bmi");
                bmiData.setComponentValue(Float.toString(bmi));
                clinicalData.add(bmiData);

            }
        }
    }

}
