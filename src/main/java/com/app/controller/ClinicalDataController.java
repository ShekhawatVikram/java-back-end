package com.app.controller;

import com.app.dto.ClinicalDataRequest;
import com.app.models.ClinicalData;
import com.app.models.Patient;
import com.app.repositories.ClinicalRepo;
import com.app.repositories.PatientRepo;
import com.app.util.BMIcalculator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class ClinicalDataController {

    @Autowired
    ClinicalRepo clinicalRepo;
    @Autowired
    PatientRepo patientRepo;

    @RequestMapping("/clinicals")
    public ClinicalData saveClinicalData(@RequestBody ClinicalDataRequest clinicalData) {
        Patient patient = patientRepo.findById(clinicalData.getPatientId()).get();
        ClinicalData data = new ClinicalData();
        data.setComponentName(clinicalData.getComponentName());
        data.setComponentValue(clinicalData.getComponentValue());
        data.setPatient(patient);
        return clinicalRepo.save(data);
    }

    @RequestMapping("/clinical/{patientId}/{componentName}")
    public List<ClinicalData> getClinicalData(@PathVariable("patientId") int patientId, @PathVariable("componentName") String componentName) {

        if (componentName.equals("bmi")) {
            componentName = "hw";
        }
        List<ClinicalData> clinicalData = clinicalRepo.findByPatientIdAndComponentNameOrderByMeasuredDateTime(patientId, componentName);
        List<ClinicalData> duplicateList = new ArrayList<>(clinicalData);
        Map<String, String> filter = new HashMap<String, String>();
        for (ClinicalData data : duplicateList) {
            BMIcalculator.bmiCalculator(clinicalData, data);
        }
        return clinicalData;
    }


}
