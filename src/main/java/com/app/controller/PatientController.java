package com.app.controller;

import com.app.models.ClinicalData;
import com.app.models.Patient;
import com.app.repositories.PatientRepo;
import com.app.util.BMIcalculator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class PatientController {
    @Autowired
    PatientRepo patientRepo;

    @RequestMapping(value = "/patients", method = RequestMethod.GET)
    public List<Patient> patientList() {
        return patientRepo.findAll();

    }

    @RequestMapping(value = "/patients/{id}")
    public Patient getPatient(@PathVariable("id") int id) {

        Optional<Patient> patient = patientRepo.findById(id);
        if (patient != null) {
            return patient.get();
        }
        return null;
    }

    @RequestMapping(value = "/patients", method = RequestMethod.POST)
    public Patient save(@RequestBody Patient patient) {
        return patientRepo.save(patient);
    }

    @RequestMapping(value = "/analyze/{id}", method = RequestMethod.GET)
    public Patient analyze(@PathVariable("id") int id) {
        Patient patient = patientRepo.findById(id).get();
        List<ClinicalData> clinicalData = patient.getClinicalData();
        List<ClinicalData> duplicateList = new ArrayList<>(clinicalData);
        Map<String, String> filter = new HashMap<String, String>();
        for (ClinicalData data : duplicateList) {
            if (filter.containsKey(data.getComponentName())) {
                clinicalData.remove(data);
                continue;
            } else {
                filter.put(data.getComponentName(), null);
            }

            BMIcalculator.bmiCalculator(clinicalData, data);
        }
        return patient;

    }


}
